var auction_state = document.querySelector('#auction-state');

function toggleAuctionState() {
	fetch('util/toggleAuctionState.php?new_val=' + auction_state.checked).then(function (response) {
		if(response.status != 200) {
			// Something went wrong!
			// The auction was not updated, so reset the auction state toggle:
			auction_state.checked = !auction_state.checked;
			alert('Database was not updated!  Contact database admin!');
		}
	});
}

// If the current auction state is true, load the switch as being toggled to true!
function getCurrentAuctionState() {
	fetch('util/getAuctionState.php').then(function(response) {
		return response.json();
	}).then(function (json) {
		if(json['status']['open'] == '1') {
			auction_state.checked = true;
		}
	});
}

// Set the switch in the correct initial position!
getCurrentAuctionState();

// Add an event handler that will call the updater to the server each time the switch is toggled!
auction_state.addEventListener('click', function (ev) {
	toggleAuctionState();
});