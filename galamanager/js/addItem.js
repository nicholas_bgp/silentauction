var file_upload = document.querySelector('#image');
var file_area = document.querySelector('#image-area');
var image_name = document.querySelector('#image-name');

file_area.addEventListener('click', function (ev) {
	file_upload.click();
});

// On file selection, do some stuff!
file_upload.addEventListener('change', function (ev) {
	console.log('changed!');
	image_name.innerHTML = this.value.replace(/^.*[\\\/]/, '');
});