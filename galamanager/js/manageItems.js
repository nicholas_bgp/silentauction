new Tablesort(document.getElementById('table-id'));

function togglePickedUp(id, new_val, that) {
	fetch('util/togglePickedUp.php?id=' + id + '&new_val=' + new_val).then(function (response) {
		if(response.status != 200) {
			// Something went wrong!
			// The auction was not updated, so reset the auction state toggle:
			that.checked = !that.checked;
			alert('Database was not updated!  Contact database admin!');
		}
	});
}

// Add an event to each checkbox that will update the status of picked_up in the database asynchronously:
var all_checkboxes = document.querySelectorAll('[name=checkbox]');

for (var i = 0, el; el = all_checkboxes[i]; ++i) {
	el.addEventListener('change', function (el) {
		togglePickedUp(this.id, this.checked, this);
	});
}