<?php include_once('util/header.php') ?>
<?php include_once('util/generatePin.php') ?>

<!DOCTYPE html>
<?php
	// Check to see if we've successfully saved a bidder before this:
	@session_start();
	if($_SESSION["saved_bidder"] === true) {
		if(isset($_SESSION["bidder_id"])) {
			echo '<div class="phantom">Bidder was saved successfully!<br>Bidder ID is: <strong>' . $_SESSION["bidder_id"] . '</strong></div>';
			$_SESSION["saved_bidder"] = false;
			unset($_SESSION['bidder_id']);
		}
	}
?>

<?php $pincode = generatePIN(); ?>

<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link rel="stylesheet" href="css/index.css">
		<title>Add Bidder</title>
	</head>
	<body>
		<div class="page-label">
			<h1>ADD BIDDER</h1>
			<a href="manageBidders.php">BACK</a>
		</div>

		<div class="bidder-info-server logos">
			<div class="bidder-pin">
				<h2>PIN NUMBER</h2>
				<span class="big-number"><?php echo $pincode ?></span>
				<input form="add-bidder-form" type="hidden" value="<?php echo $pincode ?>" name="pincode" />
			</div>
		</div>

		<form id="add-bidder-form" class="sign-in" method="post" action="util/saveBidder.php">
			<input type="text" name="name" placeholder="NAME" autofocus required /><br />
			<input type="tel" name="phone" placeholder="PHONE NUMBER" required /><br />
			<input type="email" name="email" placeholder="EMAIL" required /><br />

			<button type="submit" class="save-info-button">
				<div class="add-bidder-button no-margin"><img src="img/ic_done_white_24px.svg">SAVE INFO</div>
			</a>
		</form>
	</body>
</html>