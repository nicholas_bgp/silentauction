<?php include_once('util/header.php') ?>
<?php include_once('util/getAllItems.php') ?>
<!DOCTYPE html>
<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link rel="stylesheet" href="css/index.css">
		<link rel="stylesheet" href="css/checkbox.css">
		<title>Manage Items</title>
	</head>
	<body>
		<div class="page-label">
			<h1>MANAGE ITEMS</h1>
			<a href="dashboard.php">BACK</a>
		</div>

		<div class="open">
			<a href="addItem.php">
				<div class="add-bidder-button"><img src="img/ic_person_add_white_24px.svg">ADD ITEM</div>
			</a>
		</div>

		<table id="table-id" class="table item-table" cellpadding="0" cellspacing="0">
			<thead>
				<tr data-sort-method='thead'>
					<th>ITEM ID</th>
					<th>NAME OF PIECE</th>
					<th>DONOR</th>
					<th>VALUED AT</th>
					<th>PICKED UP</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($all_items as $item): ?>
				<tr>
					<td>
						<div style="position:absolute; color:red; cursor:pointer;" onclick="deleteRow(this.parentNode.parentNode, <?php echo $item['id'] ?>)">
							x
						</div>
						<div class="table-circle">
							<?php echo $item['id'] ?>
						</div>
					</td>
					<td contenteditable=true onchange="updateField('title', this.innerHTML, <?php echo $item['id'] ?>)">
						<?php echo $item['title'] ?>
					</td>
					<td contenteditable=true onchange="updateField('donor', this.innerHTML, <?php echo $item['id'] ?>)">
						<?php echo $item['donor'] ?>
					</td>
					<td contenteditable=true onchange="updateField('start_price', this.innerHTML, <?php echo $item['id'] ?>)">
						<?php echo $item['start_price'] ?>
					</td>
					<td>
					<?php $checked = $item['picked_up'] ? 'checked' : ''; ?>
						<div class="rkmd-checkbox checkbox-rotate checkbox-ripple">
							<label class="input-checkbox checkbox-lightBlue">
								<input type="checkbox" id="<?php echo $item['id'] ?>" name="checkbox" <?php echo $checked ?>>
								<span class="checkbox"></span>
							</label>
						</div>
					</td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</body>

	<script src='js/tablesort.min.js'></script>
	<script src="js/manageItems.js"></script>
	
	<script>
		function fix_onChange_editable_elements()
		{
		  var tags = document.querySelectorAll('[contenteditable=true][onChange]');//(requires FF 3.1+, Safari 3.1+, IE8+)
		  for (var i=tags.length-1; i>=0; i--) if (typeof(tags[i].onblur)!='function')
		  {
		    tags[i].onfocus = function()
		    {
		      this.data_orig=this.innerHTML;
		    };
		    tags[i].onblur = function()
		    {
		      if (this.innerHTML != this.data_orig)
		        this.onchange();
		      delete this.data_orig;
		    };
		  }
		}
		fix_onChange_editable_elements();
		
		// For the record, this is a terrible way to do this.  Never do this future programmer looking at this code.
		function updateField(field, newVal, id) {
			fetch('util/updateField.php?table=items&field=' + field + '&newVal=' + newVal + '&id=' + id)
				.then(function(responseObj) {
					if (responseObj.status != '200') {
						alert('Failed to save change to the database!');
					} else {
						console.log('status: ' + responseObj.status);
					}
			});
		}
		
		function deleteRow(that, id) {
			console.log('deleting ' + id);
			if(confirm('Are you sure that you want to delete record ' + id + '?')) {
				console.log('Oh, we are super serious about this.');
			
				fetch('util/deleteItem.php?id=' + id)
					.then(function(responseObj) {
						if (responseObj.status != '200') {
							alert('Failed to delete item!');
						} else {
							console.log('status: ' + responseObj.status);
							
							// Now we need to remove the row from the the table...
							that.remove();
						}
				});
			}
		}
	</script>
</html>