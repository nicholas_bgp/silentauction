<!DOCTYPE html>

<?php include_once('util/header.php') ?>

<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link rel="stylesheet" href="css/index.css">
		<title>Results</title>
		
		<style>
		.img-card {
			box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
			border-radius: 7px;
			box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
			border-radius: 7px;
			text-align: center;
		}
		.weighter {
			font-weight: 100;
		}
		</style>
	</head>
	<body>
		<div class="page-label">
			<h1>RESULTS</h1>
			<!-- <a href="dashboard.php">BACK</a> -->
		</div>
		
		
		<div style="width:100%; height: 100%;">
			<div style="display: inline-block;">
				<div class="img-card">
					<img src="https://gala.thewallbyu.com/util/uploads/miracle.jpg">
					<br />
					<h3>VISION TO THE YOUTH BARTHOLEMEW</h3>
					<em>DONATED BY: NESTEROV, M.V.</em>
				</div>
			</div>
			
			<div style="display: inline-block;height: 100%;width: 48%;position: absolute;">
				<div class="img-card" style="width: 100%;">
					<div class="">
						<h2>ITEM</h2>
						<h1 class="weighter">167</h1>
					</div>
					<div class="">
						<h2>HIGHEST BIDDER</h2>
						<h1 class="weighter">47</h1>
					</div>
					<div class="">
						<h2>BID AMOUNT</h2>
						<h1 class="weighter">$1000</h1>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>