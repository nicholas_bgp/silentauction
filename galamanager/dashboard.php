<?php include_once('util/header.php') ?>
<!DOCTYPE html>
<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link rel="stylesheet" href="css/index.css">
		<title>Dashboard</title>
	</head>
	<body>
		<div class="page-label">
			<h1>DASHBOARD</h1>
		</div>

		<div class="open">
			<input type="checkbox" id="auction-state" name="auction-state" class="switch-input">
			<label for="auction-state" class="switch-label"><h2>AUCTION IS CURRENTLY <span class="toggle--on">OPEN</span><span class="toggle--off">CLOSED</span></h2></label>
		</div>

		<div class="card-options">
			<a href="manageBidders.php">
				<div class="card-option">
					<img src="img/ic_people_black_24px.svg">
					<h2>MANAGE BIDDERS</h2>
				</div>
			</a>
			<a href="manageItems.php">
				<div class="card-option">
					<h2>MANAGE ITEMS</h2>
					<img src="img/ic_color_lens_black_24px.svg">
				</div>
			</a>
			<a href="results.php" target="_blank">
				<div class="card-option">
					<h2>VIEW RESULTS</h2>
					<img src="img/ic_insert_chart_black_24px.svg">
				</div>
			</a>
			<a href="stats.php" target="_blank">
				<div class="card-option">
					<h2>VIEW STATS</h2>
					<img src="img/ic_insert_chart_black_24px.svg">
				</div>
			</a>
		</div>


		<script src="js/dashboard.js"></script>
	</body>
</html>