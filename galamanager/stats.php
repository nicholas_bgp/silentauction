<!DOCTYPE html>
<html>
<head>
  <title>Stats</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" href="css/index.css">
  <!-- Plotly.js -->
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>

<body>
  	<div class="page-label">
		<h1>STATS</h1>
	</div>
  
  <center>
  	<div id="myDiv" style="width: 50%; height: 50%;"><!-- Plotly chart will be drawn inside this DIV --></div>
  	
  	<div id="myOtherDiv" style="width: 50%; height: 50%;"><!-- Plotly chart will be drawn inside this DIV --></div>
  	
  	<div id="">
  		<h2>EXPECTED PROFIT</h2>
  		<h1 id="expected"></h1>
  	</div>
  	
  	<div id="">
  		<h2>MAX BID</h2>
  		<h1 id="max-bid"></h1>
  	</div>
    	<div id="">
  		<h2>MIN BID</h2>
  		<h1 id="min-bid"></h1>
  	</div>
    	<div id="">
  		<h2>AVERAGE BID</h2>
  		<h1 id="avg-bid"></h1>
  	</div>
  </center>
  <script>
	fetch('util/getStats.php').then(function(response) {
		return response.json();
	}).then(function (json) {
		
		var data = json.bids;
		var dater = [];
		
		// Initialize and fill the dater array:
		for(var i = 0, bid; bid = data[i]; ++i) {
			if (bid.user_id === 6) {
				continue;
			}
		
			if (dater[bid.item_id] === undefined) {
				dater[bid.item_id] = 1;
			} else {
				dater[bid.item_id] = dater[bid.item_id] + 1;
			}
		}
		
		var data = [{
			x: Object.keys(dater), //['giraffes', 'orangutans', 'monkeys'],
			y: Object.values(dater),
			type: 'bar'
		}];
		
		var layout = {
		  title: 'Number of Bids per Item',
		  
		  xaxis: {
		    title: 'Item ID'
		  },
		  yaxis: {
		    title: 'Number of Bids'
		  }
		}
		
		Plotly.newPlot('myDiv', data, layout);
		
		// Plug in the max bid:
		var maxBid = document.getElementById('max-bid');
		maxBid.innerHTML = '$' + json.max_bid;
		
		// Plug in the min bid:
		var minBid = document.getElementById('min-bid');
		minBid.innerHTML = '$' + json.min_bid;
		
		// Plug in the avg bid:
		var avgBid = document.getElementById('avg-bid');
		avgBid.innerHTML = '$' + json.avg_bid;
		
		// Now let's make a plot for monies:
		var data = json.bids;
		var dater = [];
		
		for(var i = 0, bid; bid = data[i]; ++i) {
			if (bid.user_id === 6) {
				continue;
			}
			
			dater[bid.item_id] = bid.amount;
		
			//if (dater[bid.item_id] === undefined) {
			//	dater[bid.item_id] = bid.amount;
			//} else {
			//	if (bid.amount > ) {
			//		dater[bid.item_id] = dater[bid.item_id] + 1;
			//	}
			//}
		}
		
		var data = [{
			x: Object.keys(dater), //['giraffes', 'orangutans', 'monkeys'],
			y: Object.values(dater),
			type: 'bar'
		}];
		
		var layout = {
		  title: 'Amount Bid on Each Item',
		  
		  xaxis: {
		    title: 'Item ID'
		  },
		  yaxis: {
		    title: 'Amount Bid ($)'
		  }
		}
		
		Plotly.newPlot('myOtherDiv', data, layout);
		
		// Plug in the total expected:
		var expected = document.getElementById('expected');
		expected.innerHTML = '$' + Object.values(dater).reduce((a, b) => a + b, 0);
	});
 
  </script>
</body>
</html>