<?php include_once('util/header.php') ?>
<?php include_once('util/getAllBidders.php') ?>

<!DOCTYPE html>
<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link rel="stylesheet" href="css/index.css">
		<title>Manage Bidders</title>
	</head>
	<body>
		<div class="page-label">
			<h1>MANAGE BIDDERS</h1>
			<a href="dashboard.php">BACK</a>
		</div>

		<div class="open">
			<a href="addBidder.php">
				<div class="add-bidder-button"><img src="img/ic_person_add_white_24px.svg">ADD BIDDER</div>
			</a>
		</div>

		<table id="table-id" class="table" cellpadding="0" cellspacing="0">
			<thead>
				<tr data-sort-method='thead'>
					<th>BID ID</th>
					<th>USERNAME</th>
					<th>PINCODE</th>
					<th>NAME</th>
					<th>PHONE</th>
					<th>EMAIL</th>
					<th>ADMIN</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($all_bidders as $user): ?>
				<tr>
					<td>
						<div style="position:absolute; color:red; cursor:pointer;" onclick="deleteRow(this.parentNode.parentNode, <?php echo $user['id'] ?>)">
							x
						</div>
						<div class="table-circle">
							<?php echo $user['id'] ?>
						</div>
					</td>

					<td contenteditable=true onchange="updateField('username', this.innerHTML, <?php echo $user['id'] ?>)">
						<?php echo $user['username'] ?>
					</td>
					<td contenteditable=true onchange="updateField('pincode', this.innerHTML, <?php echo $user['id'] ?>)">
						<?php echo $user['pincode'] ?>
					<td contenteditable=true onchange="updateField('name', this.innerHTML, <?php echo $user['id'] ?>)">
						<?php echo $user['name'] ?>
					</td>
					<td contenteditable=true onchange="updateField('phone', this.innerHTML, <?php echo $user['id'] ?>)">
						<?php echo $user['phone'] ?>
					</td>
					<td contenteditable=true onchange="updateField('email', this.innerHTML, <?php echo $user['id'] ?>)">
						<?php echo $user['email'] ?>
					</td>
					<td contenteditable=true onchange="updateField('is_admin', this.innerHTML, <?php echo $user['id'] ?>)">
						<?php echo $user['is_admin'] ?>
					</td>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</body>

	<script src='js/tablesort.min.js'></script>

	<script>
		new Tablesort(document.getElementById('table-id'));
	</script>
	
	<script>
		function fix_onChange_editable_elements()
		{
		  var tags = document.querySelectorAll('[contenteditable=true][onChange]');//(requires FF 3.1+, Safari 3.1+, IE8+)
		  for (var i=tags.length-1; i>=0; i--) if (typeof(tags[i].onblur)!='function')
		  {
		    tags[i].onfocus = function()
		    {
		      this.data_orig=this.innerHTML;
		    };
		    tags[i].onblur = function()
		    {
		      if (this.innerHTML != this.data_orig)
		        this.onchange();
		      delete this.data_orig;
		    };
		  }
		}
		fix_onChange_editable_elements();
		
		// For the record, this is a terrible way to do this.  Never do this future programmer looking at this code.
		function updateField(field, newVal, id) {
			fetch('util/updateField.php?table=users&field=' + field + '&newVal=' + newVal + '&id=' + id)
				.then(function(responseObj) {
					if (responseObj.status != '200') {
						alert('Failed to save change to the database!');
					} else {
						console.log('status: ' + responseObj.status);
					}
			});
		}
		
		function deleteRow(that, id) {
			console.log('deleting ' + id);
			if(confirm('Are you sure that you want to delete record ' + id + '?')) {
				console.log('Oh, we are super serious about this.');
			
				fetch('util/deleteUser.php?id=' + id)
					.then(function(responseObj) {
						if (responseObj.status != '200') {
							alert('Failed to delete item!');
						} else {
							console.log('status: ' + responseObj.status);
							
							// Now we need to remove the row from the the table...
							that.remove();
						}
				});
			}
		}
	</script>
</html>