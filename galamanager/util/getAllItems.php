<?php
	include_once('header.php');
	include_once('database.php');

	try {
		$connection = db_connect();

		$query = 'SELECT id, title, donor, CONCAT("$", start_price), start_price, picked_up FROM items';
		$result = $connection->query($query);
		$all_items = $result->fetchall();

	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
    		http_response_code(500);
	}
?>