<?php
	header('Content-Type: application/json');

	$table = $_GET['table'];
	$id = $_GET['id'];
	$field = $_GET['field'];
	$newVal = $_GET['newVal'];
	
	//$query = 'UPDATE ' . $table . ' SET ' . $field . ' = "' . $newVal . '" WHERE id = ' . $id;
	
	include_once('header.php');
	include_once('database.php');
	try {
		$connection = db_connect();

		$stmt = $connection->prepare('UPDATE ' . $table . ' SET ' . $field . ' = :new_val WHERE id = :id');
		//$stmt->execute(array('table' => $table, 'field' => $field, 'new_val' => $newVal, 'id' => $id));
		$stmt->execute(array('new_val' => $newVal, 'id' => $id));

		http_response_code(200);

	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
    		http_response_code(500);
	}
	
	//echo json_encode("All good in the hood");
	
?>