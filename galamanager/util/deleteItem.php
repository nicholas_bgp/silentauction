<?php
	header('Content-Type: application/json');
	include_once('header.php');
	include_once('database.php');

	$id = $_GET['id'];

	try {
		$connection = db_connect();

		$stmt = $connection->prepare('DELETE FROM items WHERE id = :id');
		$stmt->execute(array('id' => $id));

		http_response_code(200);

	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
    		http_response_code(500);
	}
?>