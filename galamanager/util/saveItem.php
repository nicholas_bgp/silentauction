<?php
	include_once('header.php');
	include_once('database.php');
	@session_start();

	try {
		$connection = db_connect();

		$target = null;

		//echo var_dump($_FILES['photo']);

		// If there is a photo:
		if($_FILES['photo']['name']) {
			// If no errors...
			if(!$_FILES['photo']['error']) {
				// Now is the time to modify the future file name and validate the file
				$new_file_name = strtolower($_FILES['photo']['tmp_name']); //rename file

				$valid_file = true;
				//if($_FILES['photo']['size'] > (1024000)) { // Can't be larger than 1 MB
				//	$valid_file = false;
				//}

				//echo 'valid_file: ' . $valid_file;

				// If the file has passed the test
				if($valid_file) {
					// Move it to where we want it to be
					//$out = move_uploaded_file($_FILES['photo']['tmp_name'], $image_path) ? 'true' : 'false';

					//$currentdir = getcwd();
					//$target = $currentdir . '/uploads/' . basename($_FILES['photo']['name']);

					$target = '/home/thewallb/public_html/gala/util/uploads/' . basename($_FILES['photo']['name']);
					move_uploaded_file($_FILES['photo']['tmp_name'], $target);
					
					$target = 'uploads/' . basename($_FILES['photo']['name']);

				}
			}
			// If there is an error...
			else {
				// Error!
			}
		}

		// Now get the info about the item stored:
		$title = $_POST['title'];
		$donor = $_POST['donor'];
		$start_price = $_POST['start_price'];

		$query = 'INSERT INTO `items`(`title`, `donor`, `start_price`, `image_path`)
		VALUES ("' . $title . '", "' . $donor . '", "' . $start_price . '", "' . $target . '")';
		$result = $connection->query($query);

		// Set a session variable to tell the addBidder.php page that everything went well!
		@session_start();
		$_SESSION["saved_item"] = true;
		
		// Now find the ID of that item we just inserted so we can tell it to the user:
		$query = 'SELECT id FROM items
		WHERE title = "' . $title . '" AND donor = "' . $donor . '" AND start_price = "' . $start_price . '" AND image_path = "' . $target . '"';
		$result = $connection->query($query);
		$result = $result->fetchAll();

		$_SESSION["item_id"] = $result[0]['id'];
		

		header('Location: ../addItem.php');
		die();

	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
	    	//echo json_encode($data);
	    	http_response_code(500);
	}
?>