<?php
	include_once('database.php');
	header('Content-type: application/json');

	try {
		$connection = db_connect();
		$stmt = $connection->prepare('SELECT open FROM auction');
		$stmt->execute();
		$result = $stmt->fetchAll();

		$data['status'] = $result[0];

		echo json_encode($data);
		http_response_code(200);

	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
	    	echo json_encode($data);
	    	http_response_code(500);
	}
?>