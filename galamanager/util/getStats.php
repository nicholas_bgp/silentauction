<?php
	include_once('database.php');
	header('Content-type: application/json');

	try {
		$connection = db_connect();
		$stmt = $connection->prepare('SELECT * FROM bids');
		$stmt->execute();
		$result = $stmt->fetchAll();
		$data['bids'] = $result;
		
		// Find the max amount bidded:
		$stmt = $connection->prepare('SELECT MAX(amount) FROM bids');
		$stmt->execute();
		$result = $stmt->fetchAll();
		$data['max_bid'] = $result[0]['MAX(amount)'];
		
		// Find the min bid:
		$stmt = $connection->prepare('SELECT MIN(amount) FROM bids');
		$stmt->execute();
		$result = $stmt->fetchAll();
		$data['min_bid'] = $result[0]['MIN(amount)'];
		
		// Get the average bid amount:
		$stmt = $connection->prepare('SELECT AVG(amount) FROM bids');
		$stmt->execute();
		$result = $stmt->fetchAll();
		$data['avg_bid'] = $result[0]['AVG(amount)'];

		echo json_encode($data);
		http_response_code(200);

	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
	    	echo json_encode($data);
	    	http_response_code(500);
	}
?>