<?php
	header('Content-Type: application/json');
	include_once('header.php');
	include_once('database.php');

	$id = $_GET['id'];
	
	if ($id == '1') {
		// This is the root user, and cannot be deleted!
		return;
	} else {
		try {
			$connection = db_connect();
	
			$stmt = $connection->prepare('DELETE FROM users WHERE id = :id');
			$stmt->execute(array('id' => $id));
	
			http_response_code(200);
	
		} catch (PDOException $e) {
			$data = array ('data' => 'Database Error!' . $e->getMessage());
	    		http_response_code(500);
		}
	}
?>