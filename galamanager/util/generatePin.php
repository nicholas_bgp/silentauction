<?php

function generatePIN($digits = 4){
    $i = 0;
    $pin = "";
    while($i < $digits){
        //generate a random number between 0 and 9.
        $pin .= mt_rand(0, 9);
        ++$i;
    }
    return $pin;
}

?>