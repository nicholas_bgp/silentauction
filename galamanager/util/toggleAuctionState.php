<?php
	include_once('header.php');
	include_once('database.php');

	try {
		$connection = db_connect();

		// Toggle the auction on or off:
		$new_val = $_GET['new_val'];

		$query = 'UPDATE `auction` SET `open`=' . $new_val . ' WHERE 1';
		$result = $connection->query($query);
		//$result = $result->fetchall();

		// All is well!
		http_response_code(200);

	} catch (PDOException $e) {
    		http_response_code(500);
	}
?>