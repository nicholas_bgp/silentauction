<?php
	include_once('database.php');

	if (isset($_POST)) {
		// Check to see if the user is in the database and if we have the rights to do what we're trying to do!
		//echo 'checking the database...';

		$username = $_POST['username'];
		$pin = $_POST["pin-number"];

		try {
			$connection = db_connect();
			//echo 'successfully connected!  Checking if ' . $username . ' is in database and is admin...<br />';

			$query = 'SELECT * FROM users WHERE username = "' . $username . '" AND pincode = "' . $pin . '"';
			$result = $connection->query($query);
			$result = $result->fetchall();

			if (count($result) === 1) {			
				// Set the session variable to indicate we are authenticated!
				@session_start();
				$_SESSION["logged_in"] = true;
				
				//echo 'Successfully authenticated! Redirecting!';
				
				//var_dump($result[0]);
				
				if ($result[0]['is_admin'] === 1) {
					//echo 'is an admin!';
					$_SESSION["is_admin"] = true;
					header("Location: ../dashboard.php");
					die();
				} else {
					//echo 'is not an admin!';
					header("Location: https://gala.thewallbyu.com");
					die();
				}
			} else {
				header("Location: ../index.html");
				die();
			}

		} catch (PDOException $e) {
			//echo "Database error! " . $e->getMessage();
		}
	}
	else {
		// We are not posting, so don't do anything!
	}
?>