<?php
	include_once('header.php');
	include_once('database.php');

	try {
		$connection = db_connect();

		$query = 'SELECT * FROM users';
		$result = $connection->query($query);
		$all_bidders = $result->fetchall();

	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
    		http_response_code(500);
	}
?>