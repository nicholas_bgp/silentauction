<?php
	include_once('header.php');
	include_once('database.php');

	try {
		$connection = db_connect();

		// Toggle the auction on or off:
		$id = $_GET['id'];
		$new_val = $_GET['new_val'];

		$query = 'UPDATE `items` SET `picked_up`=' . $new_val . ' WHERE id = ' . $id;
		$result = $connection->query($query);
		//$result = $result->fetchall();

		// All is well!
		http_response_code(200);

	} catch (PDOException $e) {
		//echo $e->getMessage();
	    	http_response_code(500);
	}
?>