<?php
	include_once('header.php');
	include_once('database.php');

	try {
		$connection = db_connect();

		$pincode = $_POST['pincode'];

		$name = $_POST['name'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];

		$query = 'INSERT INTO `users`(`pincode`, `name`, `phone`, `email`)
		VALUES ("' . $pincode . '", "' . $name . '", "' . $phone . '", "' . $email . '")';
		//echo $query;
		$result = $connection->query($query);

		// Set a session variable to tell the addBidder.php page that everything went well!
		@session_start();
		$_SESSION["saved_bidder"] = true;
		
		// Now find the ID of that bidder we just inserted so we can tell it to the actual bidder:
		$query = 'SELECT id FROM users
		WHERE name = "' . $name . '" AND phone = "' . $phone . '" AND pincode = "' . $pincode . '" AND email = "' . $email . '"';
		//echo '<br>' . $query . '<br>';
		$result = $connection->query($query);
		$result = $result->fetchAll();
		
		//var_dump($result);
		
		$_SESSION["bidder_id"] = $result[0]['id'];
		
		//echo $_SESSION["bidder_id"];

		header('Location: ../addBidder.php');
		die();

	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
    	echo json_encode($data);
    		http_response_code(500);
	}
?>