<?php
	// Let's go ahead and grab all the items out of the database along with the winning bids
	
	include_once('util/header.php');
	include_once('util/database.php');


	try {
		$connection = db_connect();

		$stmt = $connection->prepare('SELECT * FROM items ORDER BY id');
		$stmt->execute();
		$items = $stmt->fetchAll();
		
		// Now that we have all items, let's find the highest bids:
		
		//var_dump($result);
		
		foreach ($items as $key => $item) {
			$stmt = $connection->prepare('SELECT MAX(amount), user_id FROM `bids` WHERE `bids`.`item_id` = :item_id');
			$stmt->execute(array('item_id' => $item['id']));
			$highest = $stmt->fetchAll();
			$highest = $highest[0];
			$items[$key]['highest_bid'] = $highest['MAX(amount)'];
			
			// Now find the winning bidder:
			$stmt = $connection->prepare('SELECT user_id FROM `bids` WHERE `bids`.`item_id` = :item_id AND `bids`.`amount` = :amount');
			$stmt->execute(array('item_id' => $item['id'], 'amount' => $highest['MAX(amount)']));
			$highest_bidder_guy = $stmt->fetchAll();
			$highest_bidder_guy = $highest_bidder_guy[0];
			
			$items[$key]['winning_bidder'] = $highest_bidder_guy['user_id'];
			
			//echo $highest['MAX(amount)'] . '<br>';
		}


	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
		//var_dump($data);
	}
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/lory.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" href="css/index.css">
	<title>Results</title>
	
	<style>
	html, body {
	    height: 100%;
	}
	
	.slider {
	    position: relative;
	    /*width: 100%;*/
	    height: 100%;
	    margin: 0 auto 40px;
	    -webkit-user-select: none;
	    -khtml-user-select: none;
	    -moz-user-select: -moz-none;
	    -o-user-select: none;
	    user-select: none;
	}
	
	.js_slide {
		height: 100%;
		/*width: 100%;*/
		color: black;
	}
	.frame {
		height: 100%;
		width: 100%;
	}
	
	ul {
		height: 100%;
		/*width: 100%;*/
	}
	
	.active {
	    font-size: medium;
	    line-height: normal;
	}
	/*	
	.img-card {
		box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
		border-radius: 7px;
		box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
		border-radius: 7px;
		text-align: center;
		position: absolute;
	}*/
	
	.weighter {
		font-weight: 100;
		font-size: 71px;
	}
	
	.img-card img {
		max-height: 700px;
		max-width: 700px;
	}
	.img-card {
		text-align: center;
		display: inline-block;
		box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
		border-radius: 7px;
		box-shadow: 0 0 2px 0 rgba(0,0,0,0.12), 0 2px 2px 0 rgba(0,0,0,0.24);
		border-radius: 7px;
		padding: 75px;
	}

	</style>
</head>
<body>
	<div class="page-label">
		<h1>RESULTS</h1>
	</div>

	<div class="slider js_slider js_simple">
		<div class="frame js_frame">
			<ul class="slides js_slides">
				<?php foreach($items as $item) : ?>
					<li class="js_slide">
						<div style="display: table; height: 100%; width: 100%; text-align: center">
							<div class="img-card">
								<?php if(trim($item['image_path']) != '') : ?>
									<img src="https://gala.thewallbyu.com/util/<?php echo $item['image_path'] ?>">
								<?php endif ?>

								<?php if(trim($item['title']) != '') : ?>
									<h3 style="text-transform: uppercase;"><?php echo $item['title'] ?></h3>
								<?php endif ?>
								
								<?php if(trim($item['donor']) != '') : ?>
									<em style="text-transform: uppercase;">DONATED BY: <?php echo $item['donor'] ?></em>
								<?php endif ?>
							</div>
							
							<div style="display: inline-block; position: absolute; padding-left: 150px">
								<div>
									<h2>ITEM</h2>
									<h1 class="weighter"><?php echo $item['id'] ?></h1>
								</div>
								<?php if(trim($item['winning_bidder']) != '') : ?>
									<div>
										<h2>HIGHEST BIDDER</h2>
										<h1 class="weighter"><?php echo $item['winning_bidder'] ?></h1>
									</div>
								<?php endif ?>
								<?php if(trim($item['highest_bid']) != '') : ?>
									<div>
										<h2>BID AMOUNT</h2>
										<h1 class="weighter">$<?php echo $item['highest_bid'] ?></h1>
									</div>
								<?php endif ?>
							</div>
						</div>
					</li>
				<?php endforeach ?>
			</ul>
		</div>
		<span class="js_prev prev">
			<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5"><g><path fill="#2E435A" d="M302.67 90.877l55.77 55.508L254.575 250.75 358.44 355.116l-55.77 55.506L143.56 250.75z"/></g></svg>
		</span>
		<span class="js_next next">
			<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5"><g><path fill="#2E435A" d="M199.33 410.622l-55.77-55.508L247.425 250.75 143.56 146.384l55.77-55.507L358.44 250.75z"/></g></svg>
		</span>
	</div>
	<script src="js/lory.js"></script>
	
	<script>
		document.addEventListener('DOMContentLoaded', function () {
			var simple = document.querySelector('.js_simple');
			
			lory(simple, {
				infinite: 1
			});
		});
	</script>
</body>
</html>