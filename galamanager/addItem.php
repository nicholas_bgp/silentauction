<?php
	//Check to see if we've successfully saved an item before this:
	@session_start();
	if($_SESSION["saved_item"] === true) {
		echo '<div class="phantom">Item was saved successfully!<br>Item ID is: <strong>' . $_SESSION["item_id"] . '</strong></div>';
		$_SESSION["saved_item"] = false;
		unset($_SESSION['item_id']);
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<link rel="stylesheet" href="css/index.css">
		<title>Add Item</title>
	</head>
	<body>
		<div class="page-label">
			<h1>ADD ITEM</h1>
			<a href="manageItems.php">BACK</a>
		</div>

		<div class="bidder-info-server logos">
			<div class="image-landing" id="image-area">
				<img src="img/ic_image_black_24px.svg"><br />
				<strong id="image-name">UPLOAD IMAGE</strong>
				<input form="add-item-form" type="file" id="image" class="hidden" name="photo" />
			</div>
		</div>

		<form id="add-item-form" class="sign-in" method="post" action="util/saveItem.php" enctype="multipart/form-data">
			<input type="text" name="title" placeholder="NAME OF PIECE" autofocus required /><br />
			<input type="text" name="donor" placeholder="NAME OF DONOR" required /><br />
			<input type="number" name="start_price" placeholder="VALUE" min="0" required /><br />

			<button type="submit" class="save-info-button">
				<div class="add-bidder-button no-margin"><img src="img/ic_done_white_24px.svg">SAVE INFO</div>
			</a>
		</form>
	</body>

	<script src="js/addItem.js"></script>

</html>