<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Silent Auction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdb.min.css" rel="stylesheet">
    <link href="css/client.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row-fluid">
            <h2 class="col-md-offset-2 col-md-8 h2-responsive wow fadeIn" data-wow-delay="0.2s"><br><strong>THE GALA IS CLOSED</strong></h2>
        </div>

        <section id="about" class="text-center wow fadeIn" data-wow-delay="0.2s" >
            <div class="container d-flex align-items-center height-175px1 padding-20">
              <div class="row">
                <div class="col right-border">
                    <img class="img-fluid" src="img/acacia-logo.svg">
                </div>
                <div class="col">
                    <img class="img-fluid" src="img/the-wall-logo.svg">
                </div>
              </div>
            </div>

            <div class="card card-info text-center z-depth-2">
	        <div class="card-block">
	            <p class="white-text text-uppercase">Unfortunately, the Gala is currently closed.  You can only login when the Gala is open!</p>
	        </div>
	    </div>
	    <a href="index.php" type="button" class="btn btn-primary btn-lg btn-block">Try Logging In</a>
	
        </section>
    </div>


    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="js/tether.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/mdb.min.js"></script>

    <script>
        //new WOW().init();
    </script>

</body>

</html>