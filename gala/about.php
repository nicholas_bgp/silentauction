<?php include_once('util/checker.php') ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Silent Auction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdb.min.css" rel="stylesheet">
    <link href="css/client.css" rel="stylesheet">
    
    <style>
    .watch-button {
	    position: absolute;
	    right: 0;
	    /*top: 40%;*/
	    background: #4285F4;
	}
    /*.card {
    	padding-bottom: 20%;
    }*/
    </style>
</head>

<body>
    <div class="container">
        <div class="row-fluid">
            <h2 class="col-md-offset-2 col-md-8 h2-responsive wow fadeIn" data-wow-delay="0.2s"><br><strong>ABOUT THIS EVENT</strong></h2>
        </div>

	<div class="card">
	    
            <div align="center">
		   <div class="row">
		     <div class="col-md-4">
		        <img class="img-fluid" style="height: 80%" src="../../img/acacia-logo.svg" alt="Acacia Logo">
		     </div>
		    </div>
		    <div class="row">
		     <div class="col-md-4">
		     	<h3 class="text-uppercase"><strong>About Acacia Shade</strong></h3>
		     </div>
		    </div>
	    </div>


	    <div class="card-block">
	        <!-- <h4 class="card-title text-uppercase">Acacia shade is dedicated to the welfare of children with disabilities in ghana.</h4> -->
	        <p class="card-text">
	        	Acacia Shade was organized in 2012 after our president and founder spent time doing humanitarian work with her family in Ghana. While there, she witnessed the heartbreaking plight of children with disabilities and the urgent need for their care and welfare. Once home, she began to put together a board of committed volunteers who shared a common vision and sense of compassion for these vulnerable children. From this, Acacia Shade was formed and committed itself to helping with the severe culture stigma and discrimination that exists towards children with physical and mental disabilities.
	        </p>
	        <p class="card-text">
Acacia Shade opened up a home to care for abandoned children with physical and mental disabilities in 2013. Beyond our home, we are working with different Ghanaian government agencies and other Ghanaian organizations in their efforts to create awareness and educate communities and build long-term solutions for integrating these children back into families, schools, and society.
		</p>
		<p class="card-text">
			<a href="http://acaciashade.org/" target="_blank">acaciashade.org</a>
	        </p>
	    </div>
	</div>

	<div class="card">
	    
            <div align="center">
		   <div class="row">
		     <div class="col-md-4">
		        <img class="img-fluid" style="height: 80%" src="../../img/the-wall-logo.svg" alt="The Wall Logo">
		     </div>
		    </div>
		    <div class="row">
		     <div class="col-md-4">
		     	<h3 class="text-uppercase"><strong>About The Wall</strong></h3>
		     </div>
		    </div>
	    </div>


	    <div class="card-block">
	        <!-- <h4 class="card-title text-uppercase"></h4> -->
		<p class="card-text">
	        	The Wall opened in January 2013, and has been serving the BYU and local Provo community ever since. It is a restaurant and entertainment venue, and includes an indoor fireplace, dining counter, outdoor patio, and a performance stage. The Wall is a great place to relax, eat great food, listen to live music, attend an event, or host your own catering event. It was just awarded Best of State Gourmet Burger for 2017. We are committed to providing the best food and experience for each of our guests.
	        </p>
	        <p class="card-text">
	        	<a href="http://thewall.byu.edu" target="_blank">thewall.byu.edu</a>
	        </p>
	    </div>
	</div>
	
	<div id="extra-space" style="padding: 50px"></div>

        <?php include_once('util/menu.php') ?>
    
    </div>


    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="js/tether.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/mdb.min.js"></script>
</body>

</html>