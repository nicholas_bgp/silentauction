<?php include_once('util/checker.php') ?>

<?php
	// Grab all the watched items for this user:
	@session_start();
	$bidder_id = $_SESSION['bidder_id'];
	
	include_once('util/database.php');
	
	try {
		$connection = db_connect();
		$stmt = $connection->prepare('SELECT * FROM `watches`, `items` WHERE `watches`.`item_id` = `items`.`id` AND `watches`.`user_id` = :bidder_id');
		$stmt->execute(array('bidder_id' => $bidder_id));
		$result = $stmt->fetchAll();
		
		
	} catch(PDOException $e) {
	
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	
	<title>Silent Auction</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/mdb.min.css" rel="stylesheet">
	<link href="css/client.css" rel="stylesheet">    
</head>

<body>

	<div class="container">
		<div class="row-fluid">
			<h2 class="col-md-offset-2 col-md-8 h2-responsive wow fadeIn" data-wow-delay="0.2s"><br><strong>MY WATCHLIST</strong></h2>
		</div>
		
		<?php foreach($result as $item) : ?>
			<div class="card text-center">
				<div class="card-block">
					<img class="img-fluid" src="../util/<?php echo $item['image_path'] ?>" alt="Card image cap">
					<h4 class="card-title text-uppercase"><?php echo $item['title'] ?></h4>
					<h6 class="card-title text-uppercase">Donated by <?php echo $item['donor'] ?></h6>
					<p class="card-text text-uppercase">
						<?php
							try {
								$connection = db_connect();
								
								// Find the highest bid for this item:
								$stmt = $connection->prepare('SELECT MAX(amount) FROM `bids` WHERE `bids`.`item_id` = :item_id');
								$stmt->execute(array('item_id' => $item['item_id']));
								$highest = $stmt->fetchAll();
								
								// now find my highest bid for this item:
								$stmt = $connection->prepare('SELECT MAX(amount) FROM `bids` WHERE `bids`.`item_id` = :item_id AND `bids`.`user_id` = :user_id');
								$stmt->execute(array('item_id' => $item['item_id'], 'user_id' => $bidder_id));
								$my_highest = $stmt->fetchAll();
								
								
							} catch(PDOException $e) {
							
							}
						?>
						<strong class="pull-left">Highest Bid</strong>
		        			<strong class="pull-right" id="highest-bid">$<?php echo $highest[0]['MAX(amount)'] ?></strong>
		        			<br />
						<strong class="pull-left">My Bid</strong>
		        			<strong class="pull-right" id="highest-bid">$<?php echo $my_highest[0]['MAX(amount)'] ?></strong>		        		</p>
		        			<div class="text-center">
							<a href="bid.php?item_id=<?php echo urlencode($item['item_id']) ?>&title=<?php echo urlencode($item['title']) ?>&donor=<?php echo urlencode($item['donor']) ?>&image_path=<?php echo urlencode($item['image_path']) ?>" class="btn btn-primary waves-effect btn-lg bid-button btn-block">Bid on this item</a>
						</div>
				</div>
			</div>
		<?php endforeach ?>
	</div>
	<div id="extra-space" style="padding: 50px"></div>
	
	<?php include('util/menu.php') ?>
	
	<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="js/tether.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/mdb.min.js"></script>
	
	<script>
		//new WOW().init();
	</script>
</body>
</html>