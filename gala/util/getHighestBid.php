<?php

	// Find the highest bid for this item:
	include_once('database.php');
	header('Content-type: application/json');
	
	$item_id = $_GET['item_id'];
	
	try {
		$connection = db_connect();
		$stmt = $connection->prepare('SELECT MAX(amount) FROM `bids` WHERE item_id = :item_id');
		$stmt->execute(array('item_id' => $item_id));
		$result = $stmt->fetchAll();
		$highest_bid = $result[0];
		
		echo json_encode($highest_bid);
		http_response_code(200);
		
	} catch(PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
	    	echo json_encode($data);
	    	http_response_code(500);
	}
?>