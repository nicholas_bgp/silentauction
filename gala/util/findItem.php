<?php
	include_once('database.php');
	
	$item_id = $_POST['item_id'];

	try {
		$connection = db_connect();

		//echo 'Looking for item ' . $item_id . '...';

		$stmt = $connection->prepare('SELECT id, active, title, donor, description, CONCAT("$", start_price), picked_up, image_path FROM items WHERE id = :id');
		$stmt->execute(array('id' => $item_id));
		
		$result = $stmt->fetchAll();

		if (count($result) === 1) {
			//echo 'item found!';
			@session_start();
			$_SESSION['item'] = $result[0];
			
			header('Location: ../viewItem.php');
			die();
		} else {
			@session_start();
			//echo 'item not found!';
			$_SESSION['not_found'] = true;
			
			header('Location: ../bid-on-item.php');
			die();
		}

	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
    		http_response_code(500);
	}
?>