<?php
	include_once('database.php');
	@session_start();
	//echo 'saving bid for user ' . $_SESSION["bidder_id"] . '...';
	
	$bidder_id = $_SESSION['bidder_id'];
	$pincode = $_POST['pin_number'];
	
	// First we need to verify the pin is correct given our bidder_id:
	
	try {
		$connection = db_connect();
		$stmt = $connection->prepare('SELECT * FROM `users` WHERE id = :bidder_id AND pincode = :pincode');
		$stmt->execute(array('bidder_id' => $bidder_id, 'pincode' => $pincode));
		$result = $stmt->fetchAll();
		
		if (count($result) == 1) {
			//echo 'found the user in the database!<br>';
			
			// So we are the correct person, now we need to make sure the requested bid is higher than the current one:
			$item_id = $_SESSION['item']['id'];
			$bid_amount = $_POST['bid_amount'];
			
			$stmt = $connection->prepare('SELECT MAX(amount) FROM `bids` WHERE item_id = :item_id');
			$stmt->execute(array('item_id' => $item_id));
			$result = $stmt->fetchAll();
			$highest_bid = $result[0];
			
			if (intval($bid_amount) <= intval($highest_bid["MAX(amount)"])) {
				//echo 'Your bid must be higher than the current highest bid!';
				$_SESSION['bid_too_low'] = true;
				
				header('Location: ../bid.php');
				die();
			}
			
			//so now add the bid to the bids table:			
			$stmt = $connection->prepare('INSERT INTO `bids`(`item_id`, `user_id`, `amount`) VALUES(:item_id, :user_id, :amount)');
			$stmt->execute(array('item_id' => $item_id, 'user_id' => $bidder_id, 'amount' => $bid_amount));
			
			//echo '<br>Successfully inserted bid into table!<br>';
			
			// Set some success session variables:
			$_SESSION['bid_saved'] = true;
			
			header('Location: ../bid.php');
			die();
			
		} else {
			//echo 'Whoops! No user found in the database!';
			$_SESSION['wrong_pin'] = true;
			
			header('Location: ../bid.php');
			die();
		}
		
	} catch(PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
		$_SESSION['error'] = true;
		//var_dump($data);
	}
?>