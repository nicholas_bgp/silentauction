<?php
	include_once('database.php');
	
	if (isset($_POST)) {
		// Check to see if the user is in the database and if we have the rights to do what we're trying to do!
		//echo 'checking the database...';

		$bidder_id = $_POST["bidder-id"];

		try {
			$connection = db_connect();
			//echo 'successfully connected!  Checking if ' . $bidder_id . ' is in database...<br />';

			$query = 'SELECT * FROM users WHERE id = "' . $bidder_id . '"';
			$result = $connection->query($query);
			$result = $result->fetchall();
			
			if (count($result) === 1) {			
				// Set the session variable to indicate we are authenticated!
				@session_start();
				$_SESSION["logged_in"] = true;
				$_SESSION["bidder_id"] = $bidder_id;
				$_SESSION["just_logged_in"] = true;
				
				//echo 'Successfully authenticated! Redirecting!';
				header("Location: ../bid-on-item.php");
				die();
			} else {
				// set an error:
				@session_start();
				$_SESSION['error_login'] = true;
				
				header("Location: ../index.php");
				die();
			}

		} catch (PDOException $e) {
			echo "Database error! " . $e->getMessage();
		}
	}
	else {
		// We are not posting, so don't do anything!
	}
?>