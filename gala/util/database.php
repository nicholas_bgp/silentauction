<?php

function db_connect() {

    $conn = new PDO('mysql:host=localhost;dbname=thewallb_gala', 'thewallb_root', 'r00tpa$$w0rd');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //This makes sure that PDO will throw PDOException objects on errors, which makes it much easier enter code hereto debug.
    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); //This disables emulated prepared statements by PHP, and switches to *true* prepared statements in MySQL.
    $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    return $conn; //Returns the connection object so that it may be used from the outside.
}

?>