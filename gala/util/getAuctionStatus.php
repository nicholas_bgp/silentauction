<?php
	//First off, we need to check the database and see if the auction is open!
	include_once('database.php');
	
	try {
		$connection = db_connect();
		$stmt = $connection->prepare('SELECT open FROM auction');
		$stmt->execute();
		$result = $stmt->fetchAll();
		$auction_state = $result[0];

	} catch(PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
	    	echo json_encode($data);
	}
	
	if ($auction_state['open'] === 0) {
		header('Location: closed.php');
		die();
	}
?>