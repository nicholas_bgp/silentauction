<?php
	$bidder_id = $_GET['bidder_id'];
	$item_id = $_GET['item_id'];
	
	include_once('database.php');
	
	try {
		$connection = db_connect();

		//echo 'Checking to see if the watch is already in the table...';

		$stmt = $connection->prepare('SELECT * FROM `watches` WHERE user_id = :user_id AND item_id = :item_id');
		$stmt->execute(array('user_id' => $bidder_id, 'item_id' => $item_id));
		$result = $stmt->fetchAll();
		
		if(count($result) === 1) {
			//echo 'found a watch, so removing watch!';
			
			$stmt = $connection->prepare('DELETE FROM `watches` WHERE user_id = :user_id AND item_id = :item_id');
			$stmt->execute(array('user_id' => $bidder_id, 'item_id' => $item_id));
		} else {
			//echo 'Adding watch to table...';
			
			$stmt = $connection->prepare('INSERT INTO `watches`(`user_id`,`item_id`) VALUES(:user_id, :item_id)');
			$stmt->execute(array('user_id' => $bidder_id, 'item_id' => $item_id));
		}
		
		http_response_code(200);

	} catch (PDOException $e) {
		$data = array ('data' => 'Database Error!' . $e->getMessage());
    		http_response_code(500);
	}
?>