<?php

	include_once('util/getAuctionStatus.php');	
	
	// Check and make sure that session variables are set, and if they aren't, redirect to the login page!
	
	@session_start();
	if(!isset($_SESSION['logged_in'])) {
		header('Location: index.php');
		die();
	}
	if(!isset($_SESSION['bidder_id'])) {
		header('Location: index.php');
		die();
	}
?>