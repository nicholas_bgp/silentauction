<div class="container fixed-bottom elegant-color-dark">
	<div class="row" style="text-align: center;">
		<a class="btn-flat waves-effect col" href="bid-on-item.php">
			<i class="fa fa-search fa-2x" aria-hidden="true"></i><br>ITEMS
		</a>
		<a class="btn-flat waves-effect col" href="watch.php">
			<i class="fa fa-heart-o fa-2x" aria-hidden="true"></i><br>WATCH
		</a>
		<a class="btn-flat waves-effect col" href="about.php">
			<i class="fa fa-info-circle fa-2x" aria-hidden="true"></i><br>ABOUT
		</a>
	</div>
</div>