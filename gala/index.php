<?php
	@session_start();
	include_once('util/getAuctionStatus.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Silent Auction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdb.min.css" rel="stylesheet">
    <link href="css/client.css" rel="stylesheet">
</head>

<body>
    <?php
    	if(isset($_SESSION['error_login'])) :
    		$_SESSION['error_login'] = null;
	    	unset($_SESSION['error_login']);
    ?>
    <div class="card card-danger text-center z-depth-2">
        <div class="card-block">
            <p class="white-text">OOPS!<br>LOOKS LIKE THAT AUCTION ID WAS NOT THE ONE</p>
        </div>
    </div>
    <?php endif ?>
    
    <div class="container">
        <div class="row-fluid">
            <h2 class="col-md-offset-2 col-md-8 h2-responsive wow fadeIn" data-wow-delay="0.2s"><br><strong>LOGIN</strong></h2>
        </div>

        <section id="about" class="text-center wow fadeIn" data-wow-delay="0.2s" >
            <div class="container d-flex align-items-center height-175px1 padding-20">
              <div class="row">
                <div class="col right-border">
                    <img class="img-fluid" src="img/acacia-logo.svg">
                </div>
                <div class="col">
                    <img class="img-fluid" src="img/the-wall-logo.svg">
                </div>
              </div>
            </div>

            <div class="container padtop">
                <div class="row">
                    <div class="col">
                        <div class="col md-form d-inline-block">
                            <form id="login-form" method="post" action="util/clientLogin.php">
                                <input type="number" name="bidder-id" class="input-alternate nopadding text-center" placeholder="ENTER YOUR AUCTION ID" autofocus />
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col">
                        <button type="submit" form="login-form" class="btn-floating btn-large blue"><i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            <!--<div class="container fixed-bottom elegant-color-dark">
              <div class="row">
                <div class="col">
                  <button class="btn-flat waves-effect"><i class="fa fa-search fa-2x" aria-hidden="true"></i><br>ITEMS</button>
                </div>
                <div class="col">
                  <button class="btn-flat waves-effect"><i class="fa fa-heart-o fa-2x" aria-hidden="true"></i><br>WATCH</button>
                </div>
                <div class="col">
                  <button class=" btn-flat waves-effect"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i><br>ABOUT</button>
                </div>
              </div>
            </div>-->
        </section>
    </div>


    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="js/tether.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/mdb.min.js"></script>

    <script>
        //new WOW().init();
    </script>

</body>

</html>