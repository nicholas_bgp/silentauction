<?php include_once('util/checker.php') ?>

<?php
	@session_start();
	
	if(isset($_GET['item_id'])) {
		$_SESSION['item']['id'] = $_GET['item_id'];
		$_SESSION['item']['title'] = $_GET['title'];
		$_SESSION['item']['donor'] = $_GET['donor'];
		$_SESSION['item']['image_path'] = $_GET['image_path'];
	}

	else if(!isset($_SESSION['item'])) {
		header('Location: bid-on-item.php');
		die();
	}
	
	$item = $_SESSION['item'];
	
	// Find the highest bid for this item:
	include_once('util/database.php');
	
	try {
		$connection = db_connect();
		$stmt = $connection->prepare('SELECT MAX(amount) FROM `bids` WHERE item_id = :item_id');
		$stmt->execute(array('item_id' => $item['id']));
		$result = $stmt->fetchAll();
		$highest_bid = $result[0];
		
		// Also check and see if the user is watching this item...
		$stmt = $connection->prepare('SELECT * FROM `watches` WHERE user_id = :user_id AND item_id = :item_id');
		$stmt->execute(array('user_id' => $_SESSION['bidder_id'], 'item_id' => $item['id']));
		$result = $stmt->fetchAll();
		
		if(count($result) === 1) {
			$watch_icon = 'fa-close';
		} else {
			$watch_icon = 'fa-heart-o';
		}
		
	} catch(PDOException $e) {
	
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Silent Auction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdb.min.css" rel="stylesheet">
    <link href="css/client.css" rel="stylesheet">    
    
    <style>
    .watch-button {
	    position: absolute;
	    right: 0;
	    /*top: 23%;*/
	    background: #4285F4;
	}
    .card {
	    padding-bottom: 20%;
	}
    </style>
</head>

<body>
	<?php
		@session_start();
		if(isset($_SESSION['bid_saved'])) { ?>
		
		    <div class="card card-success text-center z-depth-2">
		        <div class="card-block">
		            <p class="white-text text-uppercase">Bid was saved!</p>
		        </div>
		    </div>
		
		<?php
			unset($_SESSION['bid_saved']);
		}
		else if(isset($_SESSION['wrong_pin'])) { ?>
		    <div class="card card-warning text-center z-depth-2">
		        <div class="card-block">
		            <p class="white-text text-uppercase">Whoops! Wrong pin!<br>Make sure you signed in with the correct id</p>
		        </div>
		    </div>
		
		<?php
			unset($_SESSION['wrong_pin']);
		}
		else if(isset($_SESSION['bid_too_low'])) { ?>
		    <div class="card card-warning text-center z-depth-2">
		        <div class="card-block">
		            <p class="white-text text-uppercase">You must bid higher than the current highest bid!</p>
		        </div>
		    </div>
		<?php
			unset($_SESSION['bid_too_low']);
		}
		else if(isset($_SESSION['error'])) { ?>
		   <div class="card card-danger text-center z-depth-2">
		        <div class="card-block">
		            <p class="white-text text-uppercase">We were not able to save your bid! Try again!</p>
		        </div>
		    </div>
		<?php
			unset($_SESSION['error']);
		}
	?>

    <div class="container">
        <div class="row-fluid">
            <h2 class="col-md-offset-2 col-md-8 h2-responsive wow fadeIn" data-wow-delay="0.2s"><br><strong>ITEM <?php echo $item['id'] ?></strong></h2>
        </div>
        
	<div class="card">
	    <div>
	    	<img class="img-fluid" src="../util/<?php echo $item['image_path'] ?>" alt="Card image cap">
	    	<a class="btn-floating btn-action pull-right watch-button" id="watch-button"><i class="fa <?php echo $watch_icon ?>"></i></a>
	    </div>
	    
	    <div class="card-block">
	        <h4 class="card-title text-uppercase"><?php echo $item['title'] ?></h4>
	        <p class="card-text text-uppercase">
	        	<em>Donated by: <?php echo $item['donor'] ?></em>
	        </p>
	        <p class="card-text text-uppercase">
		        <strong class="pull-left">Highest Bid</strong>
		        <strong class="pull-right" id="highest-bid">$<?php echo $highest_bid['MAX(amount)'] ?></strong>
	        </p>
	    </div>
	    
	    <form method="POST" action="util/saveBid.php">
	            <div class="container padtop">
	                <div class="row">
	                    <div class="col">
	                        <div class="col md-form d-inline-block input-group">
		                    <span class="input-group-addon" style="position: absolute">$</span>
	                            <input type="number" name="bid_amount" min="0" class="input-alternate nopadding text-center" placeholder="BID AMOUNT" required autofocus />
	                            <input type="password" name="pin_number" class="input-alternate nopadding text-center" placeholder="PIN NUMBER" required />
	                        </div>	                        
	                    </div>
	                </div>
	            </div>
	
	            <div class="container">
	                <div class="row text-center">
	                    <div class="col">
	                        <button type="submit" class="btn-floating btn-large blue"><i class="fa fa-check"></i></button>
	                    </div>
	                </div>
	            </div>
	    </form>
	</div>
	
        <?php include('util/menu.php') ?>
    
    </div>

    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="js/tether.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/mdb.min.js"></script>

    <script>
        //new WOW().init();
    </script>
    
    <script>
	var highestBid = document.getElementById('highest-bid');
	// We want to make sure that the current highest bid is always accurate, so we'll poll the database every 5 seconds to update:
	function updateHighestBid() {
		$.get("util/getHighestBid.php?item_id=<?php echo $item['id'] ?>", function(data, status) {
			//console.log(data);
			//console.log(status);
			if (data['MAX(amount)'] === null) {
		        	highestBid.innerHTML = 'NO BIDS YET';
		        } else {
		        	highestBid.innerHTML = "$" + data['MAX(amount)'];
		        }
		});
	}
	    	
	var t = setInterval(updateHighestBid, 5000);
	
	// Now set up the add to watchlist button:
	var watch_button = document.getElementById('watch-button');
	watch_button.addEventListener('click', function (ev) {
		var that = this;
		$.get("util/addWatch.php?bidder_id=<?php echo $_SESSION['bidder_id'] ?>&item_id=<?php echo $item['id'] ?>", function(data, status){
			console.log(status);
			
			var icon = that.children[0];
			if (icon.classList.contains('fa-close')) {
				icon.classList.remove('fa-close');
				icon.classList.add('fa-heart-o');
			} else {
				icon.classList.remove('fa-heart-o');
				icon.classList.add('fa-close');
			}
		});
	});
    </script>

</body>

</html>