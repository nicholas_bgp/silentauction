<?php include_once('util/checker.php') ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Silent Auction</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdb.min.css" rel="stylesheet">
    <link href="css/client.css" rel="stylesheet">
</head>

<body>
    <?php
    	@session_start();
    	if(isset($_SESSION["just_logged_in"])) :
    	    unset($_SESSION["just_logged_in"]); ?>
	    <div class="card card-success text-center z-depth-2">
	        <div class="card-block">
	            <p class="white-text">LOGGED IN!<br>ENTER AN ITEM CODE TO PLACE A BID!</p>
	        </div>
	    </div>
    <?php endif ?>
    <?php if(isset($_SESSION['not_found'])) :
    	unset($_SESSION['not_found']); ?>
    	<div class="card card-warning text-center z-depth-2">
        <div class="card-block">
            <p class="white-text">OOPS! WE COULD NOT FIND THAT ITEM<br>MAKE SURE YOU HAVE THE RIGHT ITEM CODE</p>
        </div>
    </div>
    <?php endif ?>	

    <div class="container">
        <div class="row-fluid">
            <h2 class="col-md-offset-2 col-md-8 h2-responsive wow fadeIn" data-wow-delay="0.2s"><br><strong>BID ON AN ITEM</strong></h2>
        </div>

        <section class="text-center wow fadeIn" data-wow-delay="0.2s" >
            <div class="container d-flex align-items-center height-175px1 padding-20">
              <div class="row">
                <div class="col right-border">
                    <img class="img-fluid" src="../img/acacia-logo.svg">
                </div>
                <div class="col">
                    <img class="img-fluid" src="../img/the-wall-logo.svg">
                </div>
              </div>
            </div>
            
            <form method="POST" action="util/findItem.php">
	            <div class="container padtop">
	                <div class="row">
	                    <div class="col">
	                        <div class="col md-form d-inline-block">
	                            <input type="number" name="item_id" class="input-alternate nopadding text-center" placeholder="ENTER THE ITEM CODE" required autofocus />
	                        </div>
	                    </div>
	                </div>
	            </div>
	
	            <div class="container">
	                <div class="row">
	                    <div class="col">
	                        <button type="submit" class="btn-floating btn-large blue"><i class="fa fa-arrow-right"></i></button>
	                    </div>
	                </div>
	            </div>
	    </form>
            <?php include_once('util/menu.php') ?>
        </section>
    
    </div>

	
    <script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="js/tether.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/mdb.min.js"></script>

    <script>
        //new WOW().init();
    </script>

</body>

</html>