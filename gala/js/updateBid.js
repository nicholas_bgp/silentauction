var highestBid = document.getElementById('highest-bid');
// We want to make sure that the current highest bid is always accurate, so we'll poll the database every 10 seconds to update:
function updateHighestBid() {
	$.get("util/getHighestBid.php?item_id=<?php echo $item['id'] ?>", function(data, status) {
		console.log(data);
		console.log(status);
		if (data['MAX(amount)'] === 'null') {
	        	highestBid.innerHTML = "$" + data['MAX(amount)'];
	        } else {
	        	highestBid.innerHTML = 'NO BIDS YET';
	        }
	});
}
    	
var t = setInterval(updateHighestBid, 15000);