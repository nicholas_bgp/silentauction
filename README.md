## Silent Auction Website

This website has two parts, the **gala** side where the bidders log onto and bid for items and the **galamanager** side where the managers add items and moderate the bidding.  The way I did it was make both subdomains.

The gala_database.sql is included to generate the structure for the database, it has no data in it. This means that for now you'll have to create a root user yourself.

This site was made pretty hastily and I'll be coming back to it every now and again to update and redo certain parts. This is not intended for large scale use and has the understanding that the bidders aren't going to be actively trying to break the site. I'm going to rewrite it to be better, but right now it's not ready for the prime time.
